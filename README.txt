Freport(Functional Report Generation Helper) Module helps us in creating the technical documentation in an easy way by displaying all the modules, themes, fields, plugins etc in a single page.

It is useful for developers, project managers and product managers.

Enabled Reports
Activated Modules List
Enabled Theme List
Theme Regions
Stylesheets used
Scripts Used
Content Types List
Field list
Vocabularies list
Views list
User Groups list
Installation
Drush: drush en -y freport

Once module is installed and activated goto /admin/reports/freport to access the report.