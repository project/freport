<?php

/**
 * @file
 * Report template file.
 */
?>
<style type="text/css">
    @media print {
        .freport {
            box-shadow: none !important;
        }
    }
    body {
        background-color: #eee;
    }
    .container {
        max-width: 850px;
        margin: 0 auto;
        background-color: #fff;
        padding: 10px;
    }
    li {
        font-family: monospace;
        font-size: 15px;
        line-height: 30px;
    }
    .freport {
        margin: 10px auto;
        border-radius: 3px;
        box-shadow: 0px 1px 2px 1px #ccc;
    }
    .freport .item-list {
        margin: 10px;
    }
    .freport .item-list h3 {
        padding-bottom: 5px;
        border-bottom: 2px solid rgb(0, 0, 0);
    }
    .freport .item-list ul li:nth-child(odd) {
        background: rgba(238, 238, 238, .35);
        padding: 0 5px;
    }
    .freport .item-list ul li:nth-child(even) {
        padding: 0 5px;
    }
    .freport .item-list ul {}
    .freport .item-list ul li {}
    table td, table th {
        border: 1px solid rgb(204, 204, 204);
    }
</style>
<div class="container freport">
    <?php print render($page['content']); ?>
</div>
